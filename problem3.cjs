 let a = function (inventory){
    let sorted = inventory.sort(function (a, b) {
        a = a.car_model.toLowerCase();
        b= b.car_model.toLowerCase();
        if (a == b) {
            return 0;
          }
          else if(a < b) {
          return -1;
        }
        else{
          return 1;
        }
    });
    return sorted;
}
module.exports = a;